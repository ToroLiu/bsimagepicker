//
//  BSImageItem.swift
//  
//
//  Created by aecho on 2021/12/30.
//

import UIKit

public struct BSImageItem  {
    var fileUrl: NSURL
    var filename: String
}
