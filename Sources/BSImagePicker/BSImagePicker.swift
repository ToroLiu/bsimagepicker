
import UIKit

public protocol BSImagePickerDelegate: NSObjectProtocol {
    func imagePickerDidSelectMediaUrl(url: NSURL)
}

open class BSImagePicker: NSObject {
    public enum MediaType {
        case Image, Movie
        
        internal var str: String {
            switch self {
            case .Image: return "public.image"
            case .Movie: return "public.movie"
            }
        }
    }
    
    private let pickerController: UIImagePickerController
    
    private weak var presentationController: UIViewController?
    private weak var delegate: BSImagePickerDelegate?
    private var sourceList: [UIImagePickerController.SourceType] = []
    
    private var mediaTypes: [MediaType] = []
    
    public init(presentationController: UIViewController, delegate: BSImagePickerDelegate, supportSource: [UIImagePickerController.SourceType], mediaType: [MediaType] = [.Image]) {
        self.pickerController = UIImagePickerController()
        
        super.init()
        
        self.mediaTypes = mediaType
        
        self.presentationController = presentationController
        
        self.sourceList = supportSource
        self.delegate = delegate
        
        self.pickerController.delegate = self
        
        self.pickerController.allowsEditing = false
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type),
                self.sourceList.contains(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            
            if (type == .camera) {
                // 照相功能: 不要有錄影的功能。
                self.pickerController.mediaTypes = [MediaType.Image.str]
            } else {
                self.pickerController.mediaTypes = self.mediaTypes.map{ $0.str }
            }
            
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }
    
    @discardableResult
    public func present(from sourceView: UIView) -> Bool {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        var hasSource = false
        
        if let action = self.action(for: .camera, title: I18n.BS_TAKE_PHOTO) {
            alert.addAction(action)
            hasSource = true
        }
        if let action = self.action(for: .savedPhotosAlbum, title: I18n.BS_CAMERA_ROLL) {
            alert.addAction(action)
            hasSource = true
        }
        if let action = self.action(for: .photoLibrary, title: I18n.BS_PICK_GALLERY) {
            alert.addAction(action)
            hasSource = true
        }
        if (hasSource == false) {
            NSLog("hasSource == false")
            return false
        }
        
        let cancelAction = UIAlertAction(title: I18n.BS_CANCEL, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = sourceView
            alert.popoverPresentationController?.sourceRect = sourceView.bounds
            alert.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        
        self.presentationController?.present(alert, animated: true, completion: nil)
        
        return true
    }
    
    private func pick(_ controller: UIImagePickerController, didSeletMediaUrl url: NSURL) {
        controller.dismiss(animated: true) { [unowned self] in
            //
            self.delegate?.imagePickerDidSelectMediaUrl(url: url)
        }
    }
    
    private func pickDismiss(_ controller: UIImagePickerController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension BSImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //
        self.pickDismiss(picker)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        print("debug picker info: \(info)")
        
        //
        if #available(iOS 11, *) {
            if let imageUrl = info[.imageURL] as? NSURL {
                self.pick(picker, didSeletMediaUrl: imageUrl)
                return
            }
        }
        
        if let mediaUrl = info[.mediaURL] as? NSURL {
            self.pick(picker, didSeletMediaUrl: mediaUrl)
            return
        }
        
        if let origImage = info[.originalImage] as? UIImage, let imageData = origImage.jpegData(compressionQuality: 0.2) {
            //
            do {
                let uuid = UUID()
                let imageName = "\(uuid).jpg"
                
                let imageUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(imageName)
            
                try imageData.write(to: imageUrl)
                self.pick(picker, didSeletMediaUrl: imageUrl as NSURL)
                return
            } catch {
                NSLog("Error, failed to write image into temp url with error: \(error)")
            }
        }
    }
}

