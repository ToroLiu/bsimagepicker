//
//  UIStr.swift
//  
//
//  Created by aecho on 2021/12/28.
//

import Foundation

/// For localizedString of app
internal class I18n {
    
    static func str(_ key: String) -> String {
        return NSLocalizedString(key, tableName: "Localizable", bundle: Bundle.module, comment: "")
    }
    
    static func strLocalizable(_ key:String, comment: String ) -> String {
        return NSLocalizedString(key, tableName: "Localizable", bundle: Bundle.module, comment: comment)
    }
}

// MARK: - Strings of Localizable.strings
internal extension I18n {
 
    // 照相
    static let BS_TAKE_PHOTO = I18n.str("BS_TAKE_PHOTO")
    // 相機膠卷
    static let BS_CAMERA_ROLL = I18n.str("BS_CAMERA_ROLL")
    // 圖庫
    static let BS_PICK_GALLERY = I18n.str("BS_PICK_GALLERY")
    // 無法從相機，或是圖庫取得影像。
    static let BS_PHOTO_NO_SOURCE = I18n.str("BS_PHOTO_NO_SOURCE")
    // 需要相機的權限取得影像
    static let BS_CAMERA_PERMISSION_DESC = I18n.str("BS_CAMERA_PERMISSION_DESC")
    // 取消
    static let BS_CANCEL = I18n.str("BS_CANCEL")
}
